import React, { Component } from "react";
import { GlassesArr } from "./GlassesData";

export default class ItemGlasses extends Component {
  render() {
    return (
      <div>
        <div>
          <div className="container content pt-5">
            <div className="row">
              <div className="col-2 my-2">
                <button
                  onClick={() => {
                    this.props.handleChangeGlasses(GlassesArr[0]);
                  }}
                >
                  <img src="./GlassesInfo/glassesImage/v1.png" alt="" />
                </button>
              </div>
              <div className="col-2 my-2">
                <button
                  onClick={() => {
                    this.props.handleChangeGlasses(GlassesArr[1]);
                  }}
                >
                  <img src="./GlassesInfo/glassesImage/v2.png" alt="" />
                </button>
              </div>
              <div className="col-2 my-2">
                <button
                  onClick={() => {
                    this.props.handleChangeGlasses(GlassesArr[2]);
                  }}
                >
                  <img src="./GlassesInfo/glassesImage/v3.png" alt="" />
                </button>
              </div>
              <div className="col-2 my-2">
                <button
                  onClick={() => {
                    this.props.handleChangeGlasses(GlassesArr[3]);
                  }}
                >
                  <img src="./GlassesInfo/glassesImage/v4.png" alt="" />
                </button>
              </div>
              <div className="col-2 my-2">
                <button
                  onClick={() => {
                    this.props.handleChangeGlasses(GlassesArr[4]);
                  }}
                >
                  <img src="./GlassesInfo/glassesImage/v5.png" alt="" />
                </button>
              </div>
              <div className="col-2 my-2">
                <button
                  onClick={() => {
                    this.props.handleChangeGlasses(GlassesArr[5]);
                  }}
                >
                  <img src="./GlassesInfo/glassesImage/v6.png" alt="" />
                </button>
              </div>
              <div className="col-2 my-2">
                <button
                  onClick={() => {
                    this.props.handleChangeGlasses(GlassesArr[6]);
                  }}
                >
                  <img src="./GlassesInfo/glassesImage/v7.png" alt="" />
                </button>
              </div>
              <div className="col-2 my-2">
                <button
                  onClick={() => {
                    this.props.handleChangeGlasses(GlassesArr[7]);
                  }}
                >
                  <img src="./GlassesInfo/glassesImage/v8.png" alt="" />
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

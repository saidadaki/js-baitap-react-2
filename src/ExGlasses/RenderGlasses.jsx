import React, { Component } from "react";
import { GlassesArr } from "./GlassesData";
import ItemGlasses from "./ItemGlasses";

export default class RenderGlasses extends Component {
  state = {
    imgSrc: "",
    name: "",
    desc: "",
  };
  handleChangeGlasses = (glasses) => {
    this.setState({
      imgSrc: glasses["url"],
      name: glasses["name"],
      desc: glasses["desc"],
    });
  };
  render() {
    return (
      <div>
        <div
          style={{
            backgroundImage: "url('./GlassesInfo/glassesImage/000048.jpg')",
            backgroundRepeat: "no-repeat",
            backgroundSize: "cover",
          }}
        >
          <nav
            className="bg-dark
             p-3 text-white"
          >
            <h1>Try new Glasses Online</h1>
          </nav>
          <div className="container d-flex justify-content-around align-items-center mt-5">
            <div className="image-container position-relative">
              <img
                className="model"
                src="./GlassesInfo/glassesImage/model.jpg"
                alt=""
              />
              {/* State can thay doi  */}
              <div>
                <img className="glasses" src={this.state.imgSrc} alt="" />
                <div className="glassesDetail bg bg-warning">
                  <h5>{this.state.name}</h5>
                  <p className="mb-0">{this.state.desc}</p>
                </div>
              </div>
              {/* State thay doi - End  */}
            </div>
            <div>
              <img
                className="model"
                src="./GlassesInfo/glassesImage/model.jpg"
                alt=""
              />
            </div>
          </div>

          <ItemGlasses handleChangeGlasses={this.handleChangeGlasses} />
        </div>
      </div>
    );
  }
}
